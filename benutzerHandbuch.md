# Benutzerhandbuch

### Suchen nach Mechs, Gewicht, Jahr, usw.
Die wichtigen Zeichen bei der Suche sind : und ;
Der : trennt das schluesselwort vom parameter
Das ; trennt die suchpaare

* aera:CBT,Clan;
* bvmin: [0-9];
* bvmax: [0-9];
* jahrmin: [0-9]{4};
* jahrmax: [0-9]{4};
* tomin: [0-9]{2,3};
* tomax: [0-9]{2,3};
* name: name1,name2,caseinsensetive;
* type: type1,type2,casei
* limit: start,count|| count

###### Beispiel
```
aera:Clan,ISakt;bvmin:1000;tomax:65;limit:20
```